Drupal sequential nodes module:

Author - Eugene Sorochinskiy webmaster at darkguard dot net
Requires - Drupal 7


Overview:

Just a simple way to add previous/next node links to a node content.
Actual when displaying full node content 

Description

Adds two links to the previous and next node of the same type and
status. These links are added as a field to the node content so it
could be hidden or displayed in a different way in various node
displays.
Nodes are ordered in the following way: preferrable field, creation date.

Dependencies

- Token
